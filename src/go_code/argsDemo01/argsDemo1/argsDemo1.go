package main

import (
	"fmt"
	"os"
)

/**
os.Args 是个切片,可以得到所有的命令行参数
参数 顺序
*/
func main() {

	fmt.Println("命令行的参数有", len(os.Args))
	// 在命令行 中 运行  go run .\argsDemo1.go  1 2 3  会输入这些参数
	for i, arg := range os.Args {
		fmt.Printf("下标=%v , arg=%v \n", i, arg)
	}

}
