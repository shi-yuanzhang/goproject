package main

import (
	"flag"
	"fmt"
)

/**
flag包,可以方便的解析明明行参数,而且不需要顺序
*/
func main() {

	// 定义几个变量,用于接收命令行的参数值
	var user string
	var pwd string
	var host string
	var port int

	// p 用来接收参数, name 获取的指定参数 ,value  默认值, usage说明
	// 信息注册 flag.StringVar(p *string, name string, value string, usage string)
	flag.StringVar(&user, "u", "", "用户名,默认为空")
	flag.StringVar(&pwd, "p", "", "密码,默认为空")
	flag.StringVar(&host, "h", "localhost", "主机名,默认为localhost")
	flag.IntVar(&port, "port", 3306, "端口号,默认为3306")
	// 未调用flag.Parse() 解析注册,只会有默认值
	fmt.Printf("解析注册前 user=%v,pwd=%v,host=%v,port=%v \n", user, pwd, host, port)
	//转换,从os.Args的参数解析注册的flag,在flag注册好未访问前执行.
	flag.Parse()
	// 设置已经注册的flag值,将u替换成func
	//err := flag.Set("u", "func")
	//if err != nil {
	//	fmt.Println(err)
	//}
	// go  run .\flagDemo1.go  -u root -p root -h 127.0.0.1 -port 3306
	fmt.Printf("解析注册后 user=%v,pwd=%v,host=%v,port=%v", user, pwd, host, port)
}
