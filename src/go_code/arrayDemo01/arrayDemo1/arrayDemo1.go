package main

import "fmt"

/**
数组
*/
func main() {

	var arr1 [2]int
	fmt.Println(arr1)
	var arr2 = [2]int{1, 2}
	fmt.Println(arr2)
	// 省略长度
	var arr3 = [...]int{1, 2, 3, 4, 5, 6, 7}
	fmt.Println(arr3)
	// len(arr3) 获取长度
	fmt.Println("arr3 数组长度,", len(arr3))
	fmt.Println(arr3[0])
	fmt.Println(arr3[1])
	fmt.Println("数组循环 -------------------------------------------------")
	for i, index := 0, len(arr3); i < index; i++ {
		fmt.Printf("arr3  index=%v  value=%v\n", i, arr3[i])
	}
	fmt.Println("-------------------------------------------------")
	for i := range arr3 {
		fmt.Println(i)
	}
	fmt.Println("-------------------------------------------------")
	for i, i2 := range arr3 {
		fmt.Printf("index=%v  value=%v \n", i, i2)
	}

}
