package main

/**
break,终止循环. 可以结束for,switch和select的代码块
 switch没有allthrough  break意义不大
代标签的break可以跳出多层select/switch 作用域,一般用在跳出多重循环

*/

func test1() {
	i := 10
	for {
		i--
		if i == 0 {
			println("跳出break")
			break
		}
	}
}

func test2() {
	i := 5
	switch i {
	case 1:
		println("case 1:")
		break
		fallthrough

	case 2:
		println("case 2;")
		break
		fallthrough

	case 5:
		println("case 5")
		break
		fallthrough
	default:
		println("default")
	}

}

func test3() {
	//  break able跳出不会继续执行for ,一般用于跳出多重循环
	var x int = 0
able:
	for x < 3 {
		println("x层", x)

		for i := 0; i < 10; i++ {

			println("i=", i)
			if i >= 5 {
				x++
				println("跳出 able")
				break able
			}
		}

	}

	println("结束")
}

func main() {
	test3()

}
