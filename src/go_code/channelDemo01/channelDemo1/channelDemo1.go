package main

import (
	"fmt"
	"math/rand"
	"time"
)

/**
Go提供了一种称为通道的机制,用于在goroutine之间共享数据.当您作为goroutine执行 并发活动时,
需要在goroutine之间共享资源或数据,通道充当goroutine之间的管道(管道)并提供一种机制来保证同步交换.

需要在声明通道时指定数据类型.我们可以共享内置、命名、结构和引用类型的值和指针.
数据在通道上传递:在任何给定时间只有一个goroutine可以访问数据项:因此按照设计不会发生数据竞争.

根据数据交换的行为,有两种类型的通道:无缓存通道和缓存通道.无换种通道用于执行goroutine之间的同步通信,而缓冲通道用于执行异步通信.
无缓冲通道保证在发送和接收发生的瞬间执行两个goroutine之间的交换.
缓冲通道没有这样的保证.

通道由make函数创建,该函数指定chan关键字和通道的元素类型.

1.创建通道
整型无缓冲通道
Unbuffered := make(chan int)
整形有缓冲通道
buffered := make(chan int, 10)

*/
// 创建int类型通道,只能传入int类型值
var values = make(chan int)

func send() {
	rand.Seed(time.Now().UnixNano())
	value := rand.Intn(10)
	fmt.Printf("send: %v \n", value)
	time.Sleep(time.Second * 5)
	values <- value
}

func main() {

	defer close(values)
	// 从通道接收值
	go send()
	fmt.Println("wait.....")
	// 通道内没有值就会阻塞
	value := <-values
	fmt.Printf("receive: %v \n", value)
	fmt.Println("end....")

}
