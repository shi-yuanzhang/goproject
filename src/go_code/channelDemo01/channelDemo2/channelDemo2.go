package main

import "fmt"

/**
channel 遍历

*/

var c = make(chan int)

func main() {

	go func() {
		//i := 0
		//for {
		//	c <- i
		//	i++
		//}
		for i := 0; i < 2; i++ {
			c <- i
		}
		// 若不加close,则容易出现死锁
		close(c)
	}()

	// 循环
	for {
		v, ok := <-c
		if ok {
			fmt.Printf("v: %v \n", v)
		} else {
			// 不加break 死循环
			break
		}
	}

	//r := <-c
	//fmt.Printf("r: %v \n", r)
	//r = <-c
	//fmt.Printf("r: %v \n", r)
	//for v := range c {
	//	fmt.Printf("v=%v \n", v)
	//}
}
