package main

import (
	"fmt"
)

/**
管道超出指定容量和多取时会出现 dead lock错误
如果没有使用协程的情况下,channel数据去玩了,再取,就会报dead lock错误
fatal error: all goroutines are asleep - deadlock!报错
*/
var (
	mapRes = make(map[int]uint64)
	//wg sync.WaitGroup
	chanMap = make(chan interface{})
)

func calculate(n int) {
	//defer wg.Done()
	var res uint64 = 1
	for i := 1; i <= n; i++ {
		res *= uint64(i)
	}
	mapRes1 := make(map[int]uint64, 1)
	mapRes1[n] = res
	chanMap <- mapRes1
}

func main() {
	var i int = 0
	for ; i < 20; i++ {
		//wg.Add(1)
		// fatal error: concurrent map writes
		go calculate(i)
	}

	fmt.Printf("结束.............. %v \n", i)
	i1 := 0

	type mapType map[int]uint64
	for i1 < i {
		// 因为是interface 在编译时还是会认为是一个interface
		v, ok := <-chanMap
		// 使用断言类型来解决此问题
		map1, err := v.(mapType)

		if !err {
			continue
		}
		fmt.Printf("i1=%v,ok=%v \n", i1, ok)
		// 此处for range有点坑  i1++ 与range下一行时出现了问题. mapRes size =20 出现了2遍打印,而且上面的打印 i1缺少打印出11~14的值
		// 但是结果确实正常的
		if ok {
			for key, value := range map1 {
				mapRes[key] = value
				fmt.Printf("apRes[key] i=%v \n", i1)
			}
			// 将for range和 i1++分开一行打印就正常了,偶尔问题没有复现
			i1++

		} else {
			// 不加break 死循环
			continue
		}

	}

	fmt.Printf("mapRes size =%v \n", len(mapRes))
	fmt.Printf("mapRes=%v \n", mapRes)

}
