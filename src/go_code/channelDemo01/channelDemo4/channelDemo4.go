package main

import "fmt"

/**
channel在关闭后,无法写入,只能读取

*/
func main() {
	intChan := make(chan int, 2)
	intChan <- 100
	intChan <- 200
	//close(intChan)

	n1 := <-intChan
	n2 := <-intChan
	fmt.Printf("n1 %v \n", n1)
	fmt.Printf("n2 %v \n", n2)

	intChan2 := make(chan int, 100)
	for i := 0; i < 10; i++ {
		intChan2 <- i
	}

	fmt.Printf("len(intChan2)=%v \n", len(intChan2))

	// 关闭管道,否则遍历时会出现deadlock的错误
	close(intChan2)
	// 在遍历时,如果channel已经关闭,则会正常遍历数据,遍历完,就会退出遍历
	for value := range intChan2 {
		fmt.Printf("value=%v \n", value)
	}
	// cap查看容量
	// len查看实际有数据的数量
	fmt.Printf("cap(intChan2)=%v \n", cap(intChan2))
	fmt.Printf("len(intChan2)=%v \n", len(intChan2))
}
