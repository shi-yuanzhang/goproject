package main

import (
	"fmt"
	"strconv"
	"time"
)

/**
select可以解决从管道取数据的阻塞问题
*/
func main() {

	intChan := make(chan int, 10)
	for i := 0; i < 10; i++ {

		intChan <- i
	}

	strChan := make(chan string, 10)
	for i := 0; i < 10; i++ {

		strChan <- strconv.Itoa(i)
	}

	// 实际开发中,我们不好确定什么是哦胡关闭感到,可以使用select来解决
label:
	for {
		select {
		case v := <-intChan:
			time.Sleep(time.Millisecond * 200)
			fmt.Printf("intChan 读取的数据 %v \n", v)
		case s := <-strChan:
			time.Sleep(time.Millisecond * 200)
			fmt.Printf("strChan 读取的数据 %v \n", s)
		default:
			fmt.Println("结束 关闭管道 ")
			close(intChan)
			close(strChan)
			// goto 不能写在for 上 否则回继续执行for
			//goto end
			// 跳出循环
			break label
			// 程序结束
			// return
		}
	}

	// end:
	fmt.Println("结程序束.....")
}
