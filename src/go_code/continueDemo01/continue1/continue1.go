package main

import "fmt"

/**
continue 终止本次循环,进行下次循环
*/
func test1() {
	for i := 0; i < 10; i++ {

		if i%2 == 0 {
			fmt.Println("continue")
			continue
		}

		fmt.Printf("i:%v \n", i)
	}
}

func test2() {
	var i1 = 0

able:
	for i := 0; i < 10; i++ {
		i1++
		if i1%2 == 0 {
			fmt.Println("2 continue")
			continue able
		}

		fmt.Println("11111111111")
	}
}

func main() {
	test2()
}
