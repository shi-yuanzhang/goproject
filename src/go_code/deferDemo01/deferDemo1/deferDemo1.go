package main

import "fmt"

/**
defer特性
1.关键字defer用于注册延迟调用
2.这些调用知道return前才被执行.因此,可以用来做资源清理.
3.多个defer语句,按先进后出(从函数下向上顺序执行)的方式执行.
4.defer语句种的变量,在defer声明时就决定了

defer用途
1.关闭文件句柄
2.锁资源释放
3.数据库连接释放

*/
func f1() {
	fmt.Println("strart...")
	defer fmt.Println("step1....")
	defer fmt.Println("step2....")
	fmt.Println("end....")
}

func main() {

	f1()

}
