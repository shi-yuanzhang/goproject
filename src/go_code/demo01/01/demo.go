package main

import "fmt"

var tA int = 100
var tB = 200

/**
:= 只能用在函数体内声明
*/

func main() {

	fmt.Println("tA=", tA, "tB=", tB)

	var (
		aa int  = 100
		bb bool = true
	)
	fmt.Println(aa, bb)

	var xx, yy int = 100, 200
	fmt.Println(xx, yy)
	var kk, ll = 100, "Alter"
	fmt.Println(kk, ll)

	var a int
	fmt.Println(a + 1)
}
