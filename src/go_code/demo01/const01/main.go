package main

import "fmt"

// const 来定义枚举类型
// 可以在const 添加一个关键字 iota,没啊很难过的iota都会累加1,第一行的iota的默认值是0
const (
	BEIJING = 10 * iota
	SHANGHAI
	SHENZHEN
)

// iota 只能配置const使用
const (
	// 同行iota 不增加+1,都是0
	a, b = iota + 1, iota + 2
	c, d
	e, f
)

func main() {
	// 常量
	const LENGTH int = 10

	fmt.Println("LENGTH = ", LENGTH)

	fmt.Println("BEIJING = ", BEIJING)
	fmt.Println("SHANGHAI = ", SHANGHAI)
	fmt.Println("SHENZHEN = ", SHENZHEN)
	fmt.Println("a = ", a, "b = ", b, "c = ", c, "d = ", d, "e = ", e, "f = ", f)
}
