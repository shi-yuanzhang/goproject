package main

import (
	_ "flag"
	"fmt"
	myLib "go_code/demo02/initDemo1/lib"
	. "go_code/demo02/initDemo1/lib1"
	_ "go_code/demo02/initDemo1/lib2"
)

/**
_ "xxx" 匿名,不能使用包的方法,不使用不会报错,会调用init初始化函数.
. "xxx"  将函数全部导入,可以直接使用里面的函数不用xx.API调用,需注意重名.
xx "xxx"  别名,使用别名调用函数.
*/
func init() {
	fmt.Println("main 初始化....")
}

func main() {
	//lib2.LibDemo()
	myLib.LibDemo()
	Lib1Demo()
}
