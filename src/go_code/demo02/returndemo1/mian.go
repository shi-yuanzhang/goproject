package main

import "fmt"

func fool(a string, b int) int {
	fmt.Println("a=", a)
	fmt.Println("b=", b)
	c := 100

	return c
}

func fool1(a string, b int) (int, int) {
	fmt.Println("a=", a)
	fmt.Println("b=", b)
	c := 100
	d := 200
	return c, d
}

func foo2(a int, b int) (r1, r2 int) {
	fmt.Println("foo2 a=", a, "b=", b)
	r1 = a - b
	r2 = a + b
	return
}

func foo3(a int, b int) (r1 int, r2 int) {
	fmt.Println("foo2 a=", a, "b=", b)
	r1 = a - b
	r2 = a + b
	return
}

func main() {

	c := fool("31231", 1)
	fmt.Println("c=", c)
	d, e := fool1("123", 2)
	fmt.Println("d=", d, "e=", e)
	r1, r2 := foo2(1, 2)
	fmt.Println("r1=", r1, "r2=", r2)
}
