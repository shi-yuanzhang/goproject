package main

func main() {
	a := 100
	b := 100

	a++
	println(a)
	a--
	println(a)
	println(a - b)
	println(a << 2)
	println(a << 1)
	println(a >> 2)
	println(a >> 1)
}
