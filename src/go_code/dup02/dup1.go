package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {

	/**
		map 存储的一个键/值等对集合,并且提供常量时间的操作来存储、获取或测试集合中的某个元素.
		键可以式其值能够进行相等(==) 比较的任意类型,字符串式最常见的例子;
		值可以是任意类型.
		本次例子中,键的类型是字符串,值是int.
		内置的函数 make 可以用来新建map,它还可以有其他的用途.
	 */
	counts := make(map[string] int)

	/**
	 os.Stdin 从控制台中输入内容,
	 bufio.NewScanner 扫描器从程序的标准输入进行读取.每次调用input.Scan()读取下一行,并且讲结尾的换行符去掉
	 通过调用input.Text()来获取读到的内容.Scan函数在读到新行的时候返回true,在没有更多内容的时候返回false(更像是阻塞)
	 */
	input :=bufio.NewScanner(os.Stdin)

	str :=""
	for input.Scan(){
		/**
		每次map从输入读取一行内容,这一行就作为map中的键(key),对应的值递增1
		下面的语句 counts[input.Text()]++ 等价于
		line :=input.Text()
		counts[line] = counts[line] +1
		 */
		if input.Text()=="exit" {
			break
		}

		// counts[line] = counts[line] +1
		counts[input.Text()]=counts[str]+1
		str=input.Text()

	}

	// 注意:忽略input:Err() 中可能的错误
	for line,n :=range counts {
		// 像for一样,if语句中的条件部分也从不放在圆括号里面.但是程序体中需要用到大括号. 这里还可以有可选的else部分.当if条件为false的时候执行.
		if n >1{
			fmt.Printf("if %d\t%s\n",n,line)
		}else {
			fmt.Printf("else %d\t%s\n",n,line)
		}
	}


}
