package main

/**
doWhile
*/
func main() {

	var i = 10
	for {

		println(i)

		if i == 0 {
			println("break")
			break
		}
		i--
	}

}
