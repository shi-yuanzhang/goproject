package main

import (
	"fmt"
	"os"
	"strings"
)

/**
在golang中只有for循环,没有 while和do while,不过golang可以替代俩者


*/
func main() {
	//  1 ~3-1 的下标元素
	//fmt.Println(os.Args[1:3])
	for i := 0; i < len(os.Args); i++ {
		// 获取运行时候参数 go run forDemo.go 1 3 -X ?
		fmt.Println(os.Args[i])
	}

	s, sep := "", ""
	// 迭代, 第一个拿到的是索引,第二个是值
	for _, arg := range os.Args[1:] {
		s += sep + arg
		sep = " "
	}

	x := [...]int{1, 2, 3, 4, 5}
	for i, i2 := range x {
		println("i=", i, ",i2=", i2)
	}

	fmt.Println("s: " + s)
	fmt.Println(os.Args[1:])
	fmt.Println("join: " + strings.Join(os.Args[1:], " "))

	// 等价于while
	i := 10
	for i >= 1 {
		fmt.Printf("for %v>1 \n", i)
		i--
	}

	//for true {
	//
	//}

	// 无限循环
	//for{
	//	fmt.Println("FUCK")
	//
	//}

}
