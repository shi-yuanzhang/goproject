package main

import (
	"fmt"
)

/**
for range 循环  可以遍历数组、切片、字符串、map以及 通道channel,
通过for range便利的返回值有以下规律:
1.数组、切片、字符串返回索引和值
2. map 返回键和值
3.通道 channel 只返回通道内的值
*/
func main() {
	// 数组[...]省略  也可 var a = [5]int{1, 2, 3, 4, 5}
	var a = [...]int{1, 2, 3, 4, 5}

	// 省略掉索引
	for _, v := range a {
		fmt.Println("v=", v)
	}

	fmt.Println("-------------------")

	for i, v := range a {
		fmt.Println("i=", i, ",v=", v)
	}

	fmt.Println("-------------------")

	//[] 什么都不加,就是切片
	var s = []int{1, 2, 3}
	for i, v := range s {
		fmt.Println("i=", i, ",v=", v)
	}

	fmt.Println("-------------------")

	//map
	var m = make(map[string]int, 0)
	m["one"] = 1
	m["two"] = 2
	m["three"] = 3
	m["four"] = 4

	for key, value := range m {
		fmt.Println("key=", key, ",value=", value)
	}

	fmt.Println("-------------------")
	var str string = "hello"
	for i, s := range str {
		fmt.Println("i=", i, ",s=", s)

	}
	fmt.Println("-------------------")
}
