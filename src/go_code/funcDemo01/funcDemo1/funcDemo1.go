package main

import "fmt"

/**
函数由名称、参数类表和返回值类型,这些构成了函数的签名(signature)
func name(paramenter ) return_types{
	函数体
}
go语言中函数特性:

	1.go语言中有3种函数:普通的函数、匿名函数(没有名称的函数)、方法(定义在struct上的函数).receiver
	2.go语言种不允许函数重载(overload),也就是说不允许函数同名.
	3.go语言种的函数不能嵌套函数,但是可以嵌套匿名函数.
	4.函数是一个值,可以将函数赋值给变量,使得这个变量也能称为函数.
	5.函数可以作为参数传递给另一个函数.
	6.函数的返回值可以是一个函数.
	7.函数调用的时候,如果有参数传递隔给函数,则先拷贝参数的副本,再将副本传递给函数
	8.函数参数可以没有名称
*/

/**
sum(形参)(实参)
*/
func sum(a int, b int) (ret int) {
	ret = a + b
	return
}

func test1() (v1, v2 string) {
	v1 = "v1"
	v2 = "v2"
	return
}

/**
返回值覆盖
*/
func test2() (v1, v2 string) {
	v11 := "v1"
	v22 := "v2"
	return v11, v22
}

func test3(args ...int) {

	for i, arg := range args {
		fmt.Printf("index=%v,value=%v \n", i, arg)
	}
}

func main() {
	fmt.Println(sum(1, 2))

	v1, v2 := test1()
	fmt.Printf("v1=%v v2=%v\n", v1, v2)

	test3(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
}
