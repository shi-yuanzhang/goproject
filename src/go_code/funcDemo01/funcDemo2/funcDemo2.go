package main

import "fmt"

/**
函数类型

函数类型定义
type f1 = func(int, int) int
var ff f1
ff = func_name

将函数赋值给变量
func fuc_name(parameter_name type, f func(parameter_name type)) {

}

方法返回函数
func func_name(parameter_name type) return_func(type, type) return_type {
}
*/

func sum(a int, b int) int {
	return a + b
}

func sub(a int, b int) int {
	return a - b
}

func sayHello(name string) {
	fmt.Println(name)
}

// 函数可以作为参数传递给另一个函数
func test(name string, f func(string)) {
	f(name)
}

// 函数可以作为参数传递给另一个函数 并返回返回值
func test1(a int, b int, f func(int, int) int) int {
	return f(a, b)
}

// 返回返回值
func cal(operator string) func(int, int) int {
	switch operator {
	case "+":
		return sum
	case "-":
		return sub
	default:
		return nil
	}
}

func main() {
	// 函数类型定义
	type f1 = func(int, int) int
	var ff f1
	ff = sum
	r := ff(1, 2)
	fmt.Println(r)
	//  变量名 type  func名
	var ff1 f1 = sum
	r1 := ff1(2, 3)
	fmt.Println(r1)
	// 形参为函数名
	test("hello", sayHello)
	r2 := test1(3, 4, sum)
	fmt.Println(r2)

	var ff2 = cal("+")
	r3 := ff2(5, 6)
	fmt.Println(r3)

}
