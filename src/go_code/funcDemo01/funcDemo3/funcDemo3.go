package main

import "fmt"

/**

匿名函数,赋值
variable_name :=func(parameter_name type,parameter_name1 type){

				}

r1 := func(parameter_name int, parameter_name int) int {
		return a + b
	}(2, 4)

parameter_name:=""
parameter_name1:=""
f2 :=func() string{
	return parameter_name + parameter_name1
}

*/

func main() {
	// 匿名函数
	r1 := func(a int, b int) int {
		return a + b
	}(2, 4)

	fmt.Println(r1)

	sum := func(a int, b int) int {
		return a + b
	}

	r := sum(1, 2)
	fmt.Println(r)

}
