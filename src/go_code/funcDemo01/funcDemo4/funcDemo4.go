package main

import "fmt"

/**
闭包,可以理解为定义在函数内部的函数,本质上是将函数内部和外部链接起来的桥梁.
或者说是函数和其引用环境的组合体
*/

/**
闭包,在生命周期内,变量x也一直有效.
*/
func add() func(int) int {
	var x int
	return func(y int) int {
		x += y
		return x
	}
}

/**
闭包
*/
func cal(base int) (func(int) int, func(int) int) {

	add := func(a int) int {
		base += a
		return base

	}
	sub := func(a int) int {
		base -= a
		return base
	}

	return add, sub
}

func main() {
	var f = add()
	fmt.Println(f(10))
	fmt.Println(f(20))
	fmt.Println(f(30))

	var f1 = add()
	fmt.Println(f1(100))
	fmt.Println("-------------------------")
	//
	add, sub := cal(100)
	r1 := add(10)
	fmt.Println(r1)
	r2 := sub(5)
	fmt.Println(r2)
}
