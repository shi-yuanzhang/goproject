package main

import (
	"fmt"
	"time"
)

/**
Timer就是定时器的意思,可以实现一些定时操作,内部也是通过channel来实现的.
<-chan 指定的单项通道类型  只读通道
*/

func main() {
	// 1.获取当前时间
	now := time.Now()
	fmt.Printf("now=%v \n", now)

	// 2.获取相关日期
	fmt.Printf("%02d-%02d-%02d %02d:%02d:%02d \n", now.Year(), now.Month(),
		now.Day(), now.Hour(), now.Minute(), now.Second())
	fmt.Printf("年=%v \n", now.Year())
	fmt.Printf("月=%v \n", int(now.Month()))
	fmt.Printf("日=%v \n", now.Day())
	fmt.Printf("时=%v \n", now.Hour())
	fmt.Printf("分=%v \n", now.Minute())
	fmt.Printf("秒=%v \n", now.Second())

	fmt.Sprintf(now.Format("2022/06/06"))
	// Sprintf 格式化返回字符串
	dateStr := fmt.Sprintf("%02d年-%02d月-%02d日 %02d:%02d:%02d \n", now.Year(), now.Month(),
		now.Day(), now.Hour(), now.Minute(), now.Second())
	fmt.Printf("dateSt=%v \n", dateStr)

	// 获取当前时间 必须填写 2006/01/02 15:04:05 时间 golang想法诞生.  实际上获取的是当前时间
	fmt.Printf("%v \n",
		now.Format("2006/01/02 15:04:05"))

	fmt.Printf("%v \n",
		now.Format("2006-01-02"))

	// 等待2秒
	time1 := time.NewTimer(time.Second * 2)
	fmt.Printf("time1 %v \n", time1)
	// 当前时间
	t1 := time.Now()
	fmt.Printf("t1 :%v \n", t1)

	// C chan 读取 ,阻塞的直到时间到了
	t2 := <-time1.C
	fmt.Printf("t2 %v \n", t2)

}
