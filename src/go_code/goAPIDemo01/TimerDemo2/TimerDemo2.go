package main

import (
	"fmt"
	"strconv"
	"time"
)

/**
时间常量
Nanosecond  Duration = 1
	Microsecond          = 1000 * Nanosecond
	Millisecond          = 1000 * Microsecond
	Second               = 1000 * Millisecond
	Minute               = 60 * Second
	Hour

Unix时间戳 秒数
func (time) Unix() int

UnixNano时间戳 纳秒数
func (time) UnixNano() int
*/
func main() {

	now := time.Now()
	// Unix时间戳 秒数
	fmt.Printf("unix %v \n", now.Unix())
	// UnixNano时间戳 纳秒数
	fmt.Printf("UnixNano %v \n ", now.UnixNano())

	start := time.Now().Unix()
	//i := 0
	//for {
	//	i++
	//	fmt.Println(i)
	//	// time.Millisecond
	//	time.Sleep(100000000 * 1)
	//	if i == 100 {
	//		break
	//	}
	//}
	test01()
	end := time.Now().Unix()
	fmt.Printf(" 耗时 %v \n", end-start)

}

func test01() {
	str := ""

	for i := 0; i < 100000; i++ {
		str += "hello" + strconv.Itoa(i)
	}
}
