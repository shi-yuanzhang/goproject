package main

import "fmt"

/**
内置函数
1.len: 获取长度
2.new: 分配内存,主要用来分配值类型,比如float32,struct..返回的是指针
3.make: 用来分配内存,主要用来分配引用类型,比如channel、map、slice
*/
func main() {

	// new创建出来 类型为指针,值为地址  num2->地址->值是存放值的地址->值
	num1 := new(int)
	fmt.Printf("类型=%T  值=%v 地址=%v  值指向的值=%v", num1, num1, &num1, *num1)

}
