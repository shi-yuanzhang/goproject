package main

import (
	"fmt"
)

/**
错误处理
1)golang不支持传统的try ...catch ... finally
2)golang中引用的处理方式为:defer,panic,recover
3)这几个异常的使用场景可以这么简单描述:go中可以抛出一个panic的异常,
然后在defer中通过recover捕获这个异常,然后正常处理.
*/
func main() {
	test()
	fmt.Println("结束...")
}

func test() {
	// 使用defer +recover 来捕获和处理异常
	defer func() {
		// recover()内置函数,可以捕获到异常

		err := recover()
		if err != nil {
			fmt.Println("err=", err)
		}
	}()

	num1 := 10
	num2 := 0
	res := num1 / num2
	// panic: runtime error: integer divide by zero
	fmt.Println("res=", res)
}
