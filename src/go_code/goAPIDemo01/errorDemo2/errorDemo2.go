package main

import (
	"errors"
	"fmt"
)

/**
go程序中,也支持自定义错误,使用errors.New和panic内置函数
1)errors.New("错误说明"),会返回一个error类型的值,表示一个错误
2)panic内置函数,接收一个interface{}类型(万能接口)的值,作为参数,
可以接收error类型的变量,输出错误信息,并退出程序.
*/
func main() {
	err := test()
	if err != nil {
		// 输入错误,并终止程序
		panic(err)
	}

	fmt.Println("结束")
}

func test() error {
	return errors.New("错误")
}
