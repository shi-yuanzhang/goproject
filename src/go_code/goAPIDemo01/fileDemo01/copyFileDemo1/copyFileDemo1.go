package main

import (
	"fmt"
	"io/fs"
	"io/ioutil"
)

func main() {
	// 将内容读到另一个文件中
	path1 := "C:\\Users\\Administrator\\Desktop\\写文件.txt"
	path2 := "C:\\Users\\Administrator\\Desktop\\读文件.txt"
	content, err := ioutil.ReadFile(path1)
	if err != nil {
		fmt.Println(err)
	}
	// 写入到path2
	err1 := ioutil.WriteFile(path2, content, fs.ModeDir)
	if err != nil {
		fmt.Println(err1)
	}

}
