package main

import (
	"bufio"
	"fmt"
	"io"
	"io/fs"
	"os"
)

/**
拷贝文件
io.Copy(writer, reader)
*/
func main() {

	readerPath := "C:\\Users\\Administrator\\Desktop\\写文件.txt"
	readerFile, err := os.Open(readerPath)
	// 关闭句柄
	defer readerFile.Close()
	if err != nil {
		fmt.Println(err)
		return
	}
	// 读文件
	reader := bufio.NewReader(readerFile)

	writePath := "C:\\Users\\Administrator\\Desktop\\拷贝文件.txt"
	wirteFile, err1 := os.OpenFile(writePath, os.O_WRONLY|os.O_CREATE, fs.ModeDir)
	if err1 != nil {
		fmt.Println(err)
		return
	}
	// 写文件
	writer := bufio.NewWriter(wirteFile)
	// 关闭句柄
	defer wirteFile.Close()
	written, err2 := io.Copy(writer, reader)
	if err2 != nil {
		fmt.Println(err2)
	}

	fmt.Printf("拷贝结束 %v", written)
}
