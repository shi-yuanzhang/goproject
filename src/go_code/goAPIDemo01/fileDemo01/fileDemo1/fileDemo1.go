package main

import (
	"fmt"
	"os"
)

/**

文件

流:数据在数据源和程序之间的经历的路径
输入流(读):数据从数据源(文件)到程序(内存)的路径
输出流(写):数据从程序(内存)到数据源(文件)的路径
os.File

Create 创建文件
Open  打开
Close 关闭
*/

func main() {
	// 打开文件
	// file 的叫法
	//1.对象. 2.file指针 3. 文件句柄
	file, _ := os.Open("C:\\Users\\Administrator\\Desktop\\新建文本文档.txt")
	fmt.Printf("file =%v \n", file)
	err := file.Close()

	fmt.Println(err)
}
