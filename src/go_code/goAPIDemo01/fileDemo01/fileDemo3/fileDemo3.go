package main

import (
	"fmt"
	"os"
)

func main() {
	path := "C:\\Users\\Administrator\\Desktop\\写文件11.txt"
	// 判断文件是否存在
	fileInfo, err := os.Stat(path)

	// IsNotExist  为true则不存在
	if os.IsNotExist(err) {
		fmt.Println("文件不存在")
	}

	// nil 存在
	if err == nil {
		fmt.Printf("文件名=%v,文件大小=%v", fileInfo.Name(), fileInfo.Size())
	}

}
