package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

/**
统计文件内的
*/
func main() {

	path := "C:\\Users\\Administrator\\Desktop\\拷贝文件.txt"
	file, err := os.Open(path)
	if err != nil {
		fmt.Println(err)
		return
	}

	reader := bufio.NewReader(file)
	countMap := make(map[string]int)
	for {
		str, err1 := reader.ReadString('\n')
		if err1 == io.EOF {
			break
		}

		// 统计
		for _, v := range str {
			//  switch 没有 值,当作分支判断
			switch {
			case v >= 'a' && v <= 'z':
				fallthrough
			case v > 'A' && v <= 'z':
				countMap["count"] = countMap["count"] + 1
			case v >= '0' && v <= '9':
				countMap["numCount"] = countMap["numCount"] + 1
			case v == ' ' || v == '\t':
				countMap["spaceCount"] = countMap["spaceCount"] + 1
			default:
				fmt.Printf("default=%v \n", v)
				countMap["otherCount"] = countMap["otherCount"] + 1

			}

		}
	}

	fmt.Printf(" 字符 count=%v, 数字 numCount=%v, 空格 spaceCount=%v,otherCount=%v", countMap["count"], countMap["numCount"], countMap["spaceCount"], countMap["otherCount"])

}
