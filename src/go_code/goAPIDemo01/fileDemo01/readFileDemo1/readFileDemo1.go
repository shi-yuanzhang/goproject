package main

import (
	"fmt"
	"io/ioutil"
)

/**
ioutil.ReadFile 一次性将文件读取
没有显示的打开或者关闭文件,都封装在ReadFile函数中了
*/
func main() {

	file := "C:\\Users\\Administrator\\Desktop\\新建文本文档.txt"
	// byte切片
	context, err := ioutil.ReadFile(file)

	if err != nil {
		fmt.Printf("err=%v", err)
	}

	// 将byte切片转成字符串
	fmt.Printf("context =%v", string(context))
}
