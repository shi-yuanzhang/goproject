package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

/**
读文件
os.Open
bufio.NewReader
*/

func main() {

	file, _ := os.Open("C:\\Users\\Administrator\\Desktop\\新建文本文档.txt")

	// 关闭文件
	defer file.Close()
	// 带缓冲区  默认缓冲区defaultBufSize = 4096
	reader := bufio.NewReader(file)
	for {
		// ReadString 读取到哪里结束  \n 读到换行就结束
		str, err := reader.ReadString('\n')
		// io.EOF表示文件的末尾
		if err == io.EOF {
			break
		}
		fmt.Printf("info =%v", str)
	}
}
