package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

/**
file.go 中
const (
	// Exactly one of O_RDONLY, O_WRONLY, or O_RDWR must be specified.
	O_RDONLY int = syscall.O_RDONLY // 只读.
	O_WRONLY int = syscall.O_WRONLY // 只写.
	O_RDWR   int = syscall.O_RDWR   // 读写.
	// The remaining values may be or'ed in to control behavior.
	O_APPEND int = syscall.O_APPEND // 追加到文件尾部.
	O_CREATE int = syscall.O_CREAT  // 如过不存在创建一个新文件,可以与O_WRONLY组合使用.
	O_EXCL   int = syscall.O_EXCL   // O_CREAT配合使用,文件必须不存在.
	O_SYNC   int = syscall.O_SYNC   // 打开文件用于同步 I/O.
	O_TRUNC  int = syscall.O_TRUNC  // 如果可能,打开时清空文件.
)

const (
	SEEK_SET int = 0 // 相对于文件起始位置
	SEEK_CUR int = 1 // 相对于文件当前位置
	SEEK_END int = 2 // 相对于文件结尾位置
)
写文件   flag int 文件打开模式(可以组合),perm FileMode 只对linux有效
os.OpenFile(name string, flag int, perm FileMode)
*/

func main() {
	filePath := "C:\\Users\\Administrator\\Desktop\\写文件.txt"
	// 都二个参数组合 写和创建,第三个参数在windows无效
	file, err := os.OpenFile(filePath, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)

	reader := bufio.NewReader(file)

	for {
		str, err1 := reader.ReadString('\n')
		if err1 == io.EOF {
			fmt.Printf("open file err=%v \n", err1)
			break
		}
		fmt.Printf("str=%v", str)
	}

	if err != nil {
		fmt.Printf("open file err=%v \n", err)
		return
	}

	// 关闭file句柄
	defer file.Close()

	// 写入的数据
	str := "自由!民主!公平!公平!还是他妈的公平!\n"
	// 写入 使用带缓存的 *Writer
	writer := bufio.NewWriter(file)
	for i := 0; i < 10; i++ {
		writer.WriteString(str)

	}

	// Writer 带缓存,因此在调用WriteString时,会先存入到缓存中
	// 所以需要调用Flush将缓存的输入写入到文件中.
	writer.Flush()
}
