package main

import (
	"fmt"
	"runtime"
)

/**
1.runtime.Gosched
让出CPU时间片,重新等待安排任务

2. runtime.Goexit()
退出协程

3.runtime.GOMAXPROCS
CPU最大的核心数,1.5之前使用1个核心数 ,现在使用最多核心数
*/

func show(s string) {
	for i := 0; i < 5; i++ {
		if i == 2 {
			// 退出协程
			fmt.Println(s + " 退出了协程")
			runtime.Goexit()
		}
		fmt.Println(s)
	}
}

func main() {
	go show("协程阿瓦达索命")

	for i := 0; i < 2; i++ {
		fmt.Println("主线程慷慨的让出了时间片...")
		runtime.Gosched()
		fmt.Println("主线程 去你武器")
	}
}
