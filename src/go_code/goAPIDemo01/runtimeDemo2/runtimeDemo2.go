package main

import (
	"fmt"
	"runtime"
	"time"
)

/**
3.runtime.GOMAXPROCS
CPU最大的核心数,1.5之前使用1个核心数 ,现在使用最多核心数
*/

func a() {
	for i := 0; i < 10; i++ {
		fmt.Println("A:", i)
	}
}

func b() {
	for i := 0; i < 10; i++ {
		fmt.Println("B:", i)
	}
}

func main() {

	fmt.Printf("runtime.NumCPU(): %v \n", runtime.NumCPU())
	runtime.GOMAXPROCS(2)
	go a()
	go b()

	time.Sleep(time.Second)
}
