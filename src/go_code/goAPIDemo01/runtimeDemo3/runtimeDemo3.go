package main

import (
	"fmt"
	"sync"
)

/**
Mutex互斥锁实现同步
出了使用channel实现同步之外,还可以使用Mutex互斥锁的方式实现同步
*/

var m int = 0

var lock sync.Mutex

var wt sync.WaitGroup

func add() {
	defer wt.Done()
	lock.Lock()
	m += 1
	fmt.Printf("m++ %v \n", m)

	lock.Unlock()

}

func sub() {
	defer wt.Done()
	lock.Lock()
	m -= 1
	fmt.Printf("m-- %v \n", m)
	lock.Unlock()

}

func main() {

	for i := 0; i < 100; i++ {
		wt.Add(2)
		// 使用协程后,两个函数的执行顺序会不一致
		go add()
		go sub()
	}

	wt.Wait()
	fmt.Println("结束......")

}
