package main

import (
	"fmt"
	"time"
)

/**
select是Go种的一个控制结构,类似于switch语句,用于处理异步io操作.
select会监听case语句中的channel的读写操作,当case中的channel读写操作为非阻塞状态(既能读写)时,
将会触发相应的动作.

1.select是Go中的一个控制结构,类似于switch语句,用于处理异步IO操作
2.如果多个case都可以运行,select会随机公平的选出一个执行,其他不会执行.
3.如果没有可运行的case语句,且有 default 语句,那么就会执行 default 的动作.
4.如果没有可运行的case语句,且没有default语句,select将阻塞,知道某个case通信可运行.

*/

var chanInt = make(chan int, 0)
var chanStr = make(chan string)

func main() {

	go func() {
		chanInt <- 100
		chanStr <- "hello"
		//defer close(chanInt)
		//defer close(chanStr)
	}()

	for {
		select {
		case r := <-chanInt:
			fmt.Printf("chanInt: %v \n", r)
		case r := <-chanStr:
			fmt.Printf("canStr%v, \n", r)
		//  不加default  chan未close时,就会出现死锁
		default:
			fmt.Println("default")

		}

		time.Sleep(time.Second)
	}

}
