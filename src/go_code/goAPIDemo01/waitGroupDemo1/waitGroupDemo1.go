package main

import (
	"fmt"
	"sync"
)

/**
sync.WaitGroup 可以等待协程都执行结束后,主进程在结束
如果不使用WaitGroup  ,在使用协程时,主进程会直接结束掉
*/

var wg sync.WaitGroup

func hello(i int) {

	// goroutine 结束就会登记-1 等价于 wg.Add(-1)
	defer wg.Done()
	fmt.Println("Goroutine!", i)
}

func main() {
	for i := 0; i < 10; i++ {
		// 启动一个groutine就会登记+1
		wg.Add(1)
		go hello(i)
	}
	// 等待所有等级的goroutine都结束
	wg.Wait()
}
