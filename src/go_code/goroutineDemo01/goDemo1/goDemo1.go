package main

import (
	"fmt"
	"time"
)

/**
Golang中的并发是函数相互独立的能力. Goroutines是并发运行的函数.
Golang提供了Goroutines作为并发,处理操作的一种方式
*/

func show(msg string) {

	for i := 0; i < 5; i++ {
		fmt.Printf("msg : %v \n", msg)
		time.Sleep(time.Millisecond * 100)
	}

}

func main() {
	// 启动一个协程来执行
	go show("golang no.1")
	// 启动第二个协程
	go show("php no.2")
	// 第三个  主程序退出了,程序就结束了
	fmt.Println("main end .....")

}
