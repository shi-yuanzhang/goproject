package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

func responseSize(url string) {
	fmt.Println("Step :", url)
	response, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}

	defer response.Body.Close()
	fmt.Println("Step2:", url)

	body, err := ioutil.ReadAll(response.Body)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Step3:", len(body))

}

func main() {
	go responseSize("https://www.baidu.com")
	go responseSize("https://jd.com")
	time.Sleep(10 * time.Second)
}
