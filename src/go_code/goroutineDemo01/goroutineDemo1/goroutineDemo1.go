package main

import (
	"fmt"
	"runtime"
)

/**
分析思路:
1)传统的方法,就是使用一个循环,循环的判断各个数是不是素数.
2)使用并发或者并行的方式,将传统素数的任务分配给多个goroutine去完成,这时
就会使用到goroutine
*/
func main() {

	// 获取逻辑CPU
	var numCpu = runtime.NumCPU()
	fmt.Printf("CPU=%v \n", numCpu)
	// 自己设置使用多少个CPU.go1.8默认运行在多个核上,1.8前需要设置
	runtime.GOMAXPROCS(4)

}
