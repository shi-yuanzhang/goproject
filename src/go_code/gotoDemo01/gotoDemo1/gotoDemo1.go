package main

import (
	"fmt"
	"math/rand"
)

/**
goto 跳出循环,可以跳到任意位置,多层循环比较有意义
*/
func main() {
	i := rand.Int()
	if i >= 2 {
		goto END1
	} else {
		goto END
	}

END:
	if i < 2 {
		fmt.Printf("%v<2 \n", i)
	}

END1:
	if i >= 2 {
		println("%v>=2 \n", i)
	}

}
