package main

import (
	"fmt"
	"math/rand"
)

/**
if支持在 在if内赋值
if i := 10; i > 4{}
*/
func main() {
	a := 20
	b := 10
	if a > 20 {
		println("a")
	} else if b > 10 {
		println("b")
	} else {
		println("c")
	}

	// 初始化值可以写在if 内,但出现作用域,出了if else 无法使用
	if i := random(); i > 4 {
		println("if=", i)
	} else {
		println("else=", i)
	}

	i := 5
	println(i)
	var name string
	var age int
	var email string
	fmt.Println("请输入,已空格隔开")
	fmt.Scan(&name, &age, &email)
	println("name=", name)
	println("age=", age)
	println("email=", email)

}

func random() (i int) {
	i = rand.Intn(10)
	return
}

func random1() (i, i1 int) {
	i = rand.Int()
	i1 = rand.Int()
	return
}
