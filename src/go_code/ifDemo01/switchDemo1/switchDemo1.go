package main

/**
switch可以使用表达式, 默认只执行一个case,如果想执行多个需要使用fallthrough
switch {
	case i > 3:
		println("case i>3", i)
		fallthrough
	case i > 5:
		println("case i>5", i)
}
*/
func main() {

	a := 10
	switch a {
	case 2:
		println("2")
	case 10:
		println("10")
	case 5:
		println("5")
	case 15:
		println("15")
	default:
		println("default")

	}

	i := 8
	// switch可以使用表达式, 默认只执行一个case,如果想执行多个需要使用fallthrough
	// 使用了fallthrough 后面的判断不符合也会进入
	switch {
	case i > 3:
		println("case i>3", i)
		fallthrough
	case i > 10:
		println("case i>10", i)
	case i > 7:
		println("case i>7", i)
	case i > 9:
		println("case i>10", i)
	default:
		println("default", i)
	}

}
