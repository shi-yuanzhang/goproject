package main

import (
	"encoding/json"
	"fmt"
)

type Monster struct {
	Name string `json:"name"`
	Age  int    `json:"age"`
}

/**
JSON(Java Script Object Notation) 是一种轻量级的数据交互格式.易于人阅读和编写.同时也易于机器解析和生成.
JSON易于机器解析和生成,并有效提升网络传输效率,通常程序在网络传输时会先将数据(结构体,map等)序列化成json字符串,
到接收方式得到json字符串时,在反序列化恢复成原来的数据类型(结构体,map等).这种发放时已经称为了各个语言的标准.
golang -序列化->json字符串 --网络传输-->程序 --反序列化-->其他语言
*/
func main() {

	monster := Monster{"哈士奇", 4}
	// json.Marshal转换时, struct 小写开头字段(私有)识别不到
	json, _ := json.Marshal(monster)
	fmt.Printf("%v", string(json))

}
