package main

import (
	"encoding/json"
	"fmt"
)

/**
struct中  首字母大写的字段为非私有,
但是在序列化为 json转换时也会首字母大写,
通过  `json:"name"` 的方式可以自定义

*/

type Person struct {
	Name string `json:"name"`
	Age  int    `json:"age"`
}

func main() {
	person := Person{"人类1号", 24}
	// json.Marshal 里也是使用到反射了
	jsonArr, err := json.Marshal(person)
	if err == nil {
		fmt.Println("jsonArr", string(jsonArr))
	}

}
