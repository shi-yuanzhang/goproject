package main

import "fmt"

/**
map 哈希表 通过key快速查找,引用类型
*/
func main() {
	// 类型声明map,未初始化分配内存
	var map1 map[string]string
	map1 = make(map[string]string, 10)
	fmt.Println("map1=", map1)

	// make分配内存创建map
	var map2 = make(map[string]string, 10)
	fmt.Println("map2=", map2)

	// {}  直接初始化
	map3 := map[string]string{
		"key1": "value1",
		"key2": "value2",
		"key3": "value3",
		"key4": "value4"}
	fmt.Println("map3=", map3)

	// 取值
	value1 := map3["key1"]
	fmt.Println("key1=", value1)

	// 赋值
	map3["key1"] = "value"
	fmt.Println("key1=", map3["key1"])

	// value5 返回的值key的值,flag返回的是key否存在
	var value5, flag = map3["key5"]
	fmt.Printf("value5=%v,flag=%v \n", value5, flag)

	fmt.Println("-------- FOR --------")
	// 遍历
	for key, value := range map3 {
		fmt.Printf("key=%v,value=%v \n", key, value)
	}

	for key := range map3 {
		fmt.Printf("key=%v \n", key)
	}

	fmt.Println(len(map3))

}
