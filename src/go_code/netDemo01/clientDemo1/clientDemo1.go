package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
)

func main() {

	// 与服务器建立连接
	conn, err := net.Dial("tcp", "127.0.0.1:8888")
	if err != nil {
		fmt.Println("err=", err.Error())
	}
	// 从终端读取输入,发送  os.Stdin代表标准输入从[终端]
	reader := bufio.NewReader(os.Stdin)
	line, _ := reader.ReadString('\n')
	// 发送数据
	n, err1 := conn.Write([]byte(line))
	if err1 != nil {
		fmt.Println("err1=", err1.Error())
	}

	fmt.Println("n=", n)

	conn.Close()

}
