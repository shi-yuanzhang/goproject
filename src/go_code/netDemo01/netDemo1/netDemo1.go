package main

import (
	"fmt"
	"net"
)

// 打印内容
func print(conn net.Conn) {
	defer conn.Close()
	// 返回远端网络地址
	remoteAddr := conn.RemoteAddr()

	bytes := make([]byte, 1024)
	// 读取数据
	n, _ := conn.Read(bytes)
	fmt.Printf("%v 发送内容 %v \n", remoteAddr, string(bytes[:n]))
}

func main() {

	fmt.Println("服务器开始监听.....")
	//使用tcp协议    0.0.0.0 ipv4  ipv6都支持
	listen, err := net.Listen("tcp", "0.0.0.0:8888")
	if err != nil {
		fmt.Println("listen err=", err.Error())
		return
	}
	defer listen.Close()

	fmt.Println("listen", listen)
	// 返回本地网络地址
	fmt.Println("Addr", listen.Addr())
	for {
		fmt.Println("等待连接.....")
		// 接收
		conn, err1 := listen.Accept()
		if err1 != nil {
			fmt.Println("Accept err1=", err1.Error())
			continue
		}

		go print(conn)

	}

}
