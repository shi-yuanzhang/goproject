package main

import "fmt"

/**
对象的可复用设计的第一块基石,便是所谓的开闭原则(Open Closed Principle,常缩写为OCP),
虽然,go不是面向对象语言,但是也可以模拟实现这个原则.对扩展式开放的,对修改是关闭的.
*/

type Pet interface {
	eat()
	sleep()
}

type Dog struct {
}

type Cat struct {
}

func (dog Dog) eat() {
	fmt.Println("dog eat.....")
}

func (dog Dog) sleep() {
	fmt.Println("dog sleep.....")
}

func (cat Cat) eat() {
	fmt.Println("cat eat.....")
}

func (cat Cat) sleep() {
	fmt.Println("cat sleep.....")
}

type Person struct {
}

func (person Person) care(pet Pet) {
	pet.eat()
	pet.sleep()

}

func main() {
	dog := Dog{}
	cat := Cat{}

	person := Person{}
	person.care(dog)
	person.care(cat)

}
