package main

import "fmt"

/**
1.基本数据类, 变量存的就是值,也叫值类型
2.获取变量的地址,用&,比如: var num int, 获取num的地址: &num
3.指针类型,指针变量存的是一个地址,这个地址指向的空间存的才是值
比如: var ptr *int=&num  ptr就是指针,它指向int 用来存储地址
4.获取指针类型所指向的值, 使用: *,比如:var *ptr int,使用*ptr获取ptr指向的值

*/
func main() {

	var i int = 10
	fmt.Print("i的地址=", &i)

	// ptr 是指针变量
	// ptr的类型 *int
	// ptr本身的值 &i
	var ptr *int = &i
	fmt.Print("\nptr的地址=", ptr, "\nptr的值=", *ptr)
	// 修改值
	*ptr = 5
	fmt.Printf("\n i=%v", i)

}
