package main

import "fmt"

/**
指针细节说明
值类型,都有对应的指针类型,形式为 *数据类型,比如int的对应的指针就是他 *int,
	float32 对应的指针类型就是 *float,以此类推.
值类型包括:基本数据类型 int系列,float系列,bool,string、数组和结构体struct
*/
func main() {
	var num int = 9
	fmt.Printf("number address=%v \n", &num)
	var ptr *int = &num
	fmt.Println("ptr", ptr)
	// 修改影响到num的值
	*ptr = 10
	fmt.Println("num = ", num)
}
