package main

import "fmt"

/**
指针
符号 * 代表取地址的值
符号 & 代表着取地址
操作数组指针不能向C语言允许使用 ++ --  运算操作地址
*/
func main() {
	var a int = 1
	changeValue(&a)
	fmt.Println("a=", a)
	fmt.Println("--------------------")
	a, b := 10, 20
	swap(&a, &b)
	fmt.Println("a=", a, "b=", b)

}

/**
修改指针
*/
func changeValue(i *int) {
	fmt.Println("一级指针i=", &i)
	var n = i
	fmt.Println("二级指针n=", n)

	*i = 10
	fmt.Println("n=", *n)
	fmt.Println("i=", *i)

}

/**
数值交换
*/
func swap(pa *int, pb *int) {
	var temp int
	// 修改值
	temp = *pa
	*pa = *pb
	*pb = temp
}
