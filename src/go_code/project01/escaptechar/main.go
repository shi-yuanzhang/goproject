package main

import "fmt"

func main() {
	// 转义
	fmt.Println("tom\tjack")
	// \符号
	fmt.Println("\\a\\b\\c")
	// \r 回车,移动到当前行首部,并全部覆盖, 版本差异
	fmt.Println("一二三\r四")
	var num = 2 + 4 * 5
	fmt.Println(num)



}
