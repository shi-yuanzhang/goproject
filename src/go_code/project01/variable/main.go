package main

import "fmt"

/**
 *	多个全局变量声明
 */
var (
	s1 = "zsy1"
	s2 = "zsy2"
	s3 = "zsy3"
)

func main() {
	//golang变量使用方式1
	//第一种:指定变量类型,声明后若不赋值,使用默认值
	// int 默认值是0
	var i int
	fmt.Println("i=", i)

	//第二种,根据值自行判定变量类型(类型推导)
	var num = 10.11
	fmt.Println("num=", num)

	//第三种 省略var, :=左侧的变量不能是已经声明过的
	// :=  相当于 var name string name="zsy" 无法在全局变量中定义
	name := "zsy"
	fmt.Println("name=", name)

	// 一次性声明多个变量的方式1
	var n1, n2, n3 int
	fmt.Println("n1=", n1, "n2=", n2, "n3=", n3)

	// 一次性声明多个变量的方式2
	var n, name1, nx = 100, "zsy", "sss"
	fmt.Println("n=", n,
		"name=", name1,
		"nx=", nx)

	fmt.Println("s1", s1, "s2", s2, "s3", s3)

}
