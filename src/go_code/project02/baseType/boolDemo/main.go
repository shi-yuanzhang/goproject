package main

import (
	"fmt"
	"unsafe"
)

/**
布尔类型:
	1)布尔类型也叫bool类型、bool类型数据值允许取值true和false
	2)bool 类型占一个字节.
	3)bool 类型适于逻辑运算,一般用于程序流程控制[注:这个后面会详细介绍]

	使用场景:
			if条件控制语句
			for循环控制语句
 */

func main() {

	var b=false
	fmt.Println("b=",b)
	fmt.Println("b 的占用空间 =",unsafe.Sizeof(b))


}