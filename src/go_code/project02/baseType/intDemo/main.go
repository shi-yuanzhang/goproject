package main

import (
	"fmt"
	"math"
	"unsafe"
)

/*
 类型  有无符号  占用存储空间   表数范围     				备注
 int8  	有		1字节 		-128~127
 int16  有		2字节 		-2的15次方~2的15次方-1
 int32 	有		4字节 		-2的31次方~2的31次方-1
 int64  有		8字节 		-2的63次方~2的63次方-1

整数类型
 类型  有无符号  占用存储空间           表数范围     				备注
 int    有     32位系统4个字节    -2的31次方~2的31次方-1
			   64位系统8个字节    -2的63次方~2的63次方-1

 uint   无     32位系统4个字节     0~2的32次方-1
			   64位系统8个字节     0~2的64次方-1

 rune   有      与int32一样       -2的31次方~2的31次方-1        等价int32,表示一个Unicode码

 byte   无      与uint8等价       0~255 						 当要存储字符时选用byte

 根据有无符号,如果有符号 那么字节二进制的第一位的0和1表示,如果是0表示正数,1表示负数

 整型的使用细节

 1) Golang各整数类型分: 有符号和无符号,int uint的大小和系统有关
 2) Golang的整型默认声明位int型
 3) 如何在程序查看某个变量的字节大小和数据类型

 4) Golang程序中整型变量在使用时,遵守保小不保大的原则,即:在保证程序正确运行下,
	尽量使用占用空间小的数据类型.[如:年龄]
 5) bit:计算机中的最小存储单位,byte:计算机中基本存储单元.[二进制在详细说]

*/

func main() {
	var i int8 = 127
	fmt.Println("i=", i)

	var u uint16 = 255
	fmt.Println("u=", u)

	//int, uint ,rune ,byte 使用
	var a int = 1
	fmt.Println("a=", a)

	var b uint = 1
	fmt.Println("b=", b)

	var c byte = 255
	fmt.Println("c=", c)

	imax := int(math.MaxInt)
	imin := int(math.MinInt)
	println("int max=", imax, ",int min=", imin)
	// 整型使用细节
	var n1 = 100
	//查看某个变量的数据类型
	// fmt.Printf() 可以用于做格式化输出
	fmt.Printf("n1 的类型 %T \n", n1)

	// 3) 查看变量的占用字节的大小和数据类型 (使用较多)
	var n2 int64 = 10
	// unsafe.Sizeof(n2) 是unsafe包的一个函数,可以返回n2变量占用的字节数
	fmt.Printf("n2 的 类型%T  n2的字节数是 %d \t", n2, unsafe.Sizeof(n2))

	// 4)尽量使用占用空间小的数据类型
	var age byte = 100
	fmt.Println(age)
	// 5)

}
