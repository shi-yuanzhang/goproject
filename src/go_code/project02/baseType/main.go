package main

import "fmt"

/*
	基本类型:
		int int8 int16 int32 int64  对应 1 2 4 8字节
		uint uint8 uint16 uint32 uint64 uintptr   在u为无符号,表示范围更大


		byte 字符型,保存单个字符  uint8的别名,在go中没有专门的char字符类型 用byte代替

		rune int32别名  表示一个Unicode 码点

		浮点类型
		float32 float64  分别代表单精度,和双精度的浮点类型 与java相比  32为float  64为double

		string  在go中string为基本类型

		bool 布尔类型  默认false

		complex64 complex128

	基本数据类型默认值:
		在go中,数据类型都由一个默认值,当程序员没有赋值时,就会保留默认值,在go中,默认值又叫零值.

	 整型 0   浮点型  0  字符串 ""  布尔类型 false
 */

func main() {
	var a int
	var b float32
	var c float64
	var isMarried bool
	var name string
	// %v 表示按照变量的值输出
	fmt.Printf("a=%d,b=%f,c=%v,isMarried=%v name=%v",a,b,c,isMarried,name)


}
