package main

import "fmt"

/**
字符串类型:string
	字符串就是一串固定长度的字符连接起来的字符序列.
	Go的字符串是由单个字节连接起来的.
	Go语言的字符串的字节使用UTF-8编码表示Unicode文本

注意事项和细节
	1)go语言的字符串的字符使用UTF-8编码表示Unicode文本,这样Golang统一使用UTF-8编码,
	2)字符串不可以修改,但是变量可多次赋值
	3)字符串的两种表示形式
	  (1)双引号,会死别转义字符
	  (2)反引号,以字符串的原生形式输出,包括换行和特殊字符,可以实现防止攻击、输出
		 源代码等效果
 */


func main(){
	var address string="北京"
	fmt.Println("address=",address)
	str2 :="abc\nabc"
	fmt.Println(str2)
	// 当出现特殊字符时,使用Tab上面的的 反引号 ``
	str3 :=`
		func main(){:
			var address string="北京"
			fmt.Println("address=",address)
			str2 :="abc\nabc"
			fmt.Println(str2)
			// 当出现特殊字符时,使用Tab上面的的 反引号(飘号) 
			str3 :=
			fmt.Println(str3)
		}
		`
	fmt.Println(str3)

	var str="hello"+" zsy"
	str +="!"
	fmt.Println(str)

	// 拼接换行 + 号必须写在后面
	var str1="hello"+"world"+
			  "hello"

	fmt.Printf(str1)
}