package main

import (
	"fmt"
	"strconv"
)

/**
基本类型和string的转换
 */
func main() {

	var num1 int=99
	var num2 float64=23.456
	var b bool = true
	var mychar byte ='h'
	var str string //空的str

	//使用第一种方式来转换 fmt.Sprintf方法

	str= fmt.Sprintf("%d",num1)
	fmt.Printf("str type %T str=%v \n",str,str)

	str=fmt.Sprintf("%f",num2)
	fmt.Printf("str type %T str=%v \n",str,str)

	str=fmt.Sprintf("%t",b)
	fmt.Printf("str type %T str=%q \n",str,str)

	str=fmt.Sprintf("%c",mychar)
	fmt.Printf("str type %T str=%q \n",str,str)


	// 第二种方式 strconv 函数
	var num3 int=99
	var num4 float64=23.456
	var b2 bool=true
	//  将num3转成10进制
	str =strconv.FormatInt(int64(num3),10)
	fmt.Printf("str type %T str=%q \n",str,str)

	// 说明:　'f' 格式 10: 表示小数位保留10位 64:表示这个数时float64
	str=strconv.FormatFloat(num4,'f',10,64)
	fmt.Printf("str type %T str=%q \n",str,str)

	str=strconv.FormatBool(b2)
	fmt.Printf("str type %T str=%q \n",str,str)

	var num5 int=4567

	// int 转成string
	str=strconv.Itoa(num5)
	fmt.Printf("str type %T str=%q \n",str,str)

}
