package main

import (
	"fmt"
	"strconv"
)

/**
类型转换:
	Golang和java/c不同,Go在不同类型的变量之间赋值时需要要显示转换.
	也就是说Golang中数据类型不能自动转换.

基本语法:
	表达式T(v)将值v转换位类型
	T:就是数据类型,比如int32 ,int64,float32 等等
	v:就是需要转换的变量

细节说明:
	1)Go中,数据类型的转换可以是从表示范围小-->表示范围大,也可以范围大-->范围小
	2)被转换的时变量存储的数据(即值),变量本身的数据类型并没有变化!
	3)在转换中,比如将int64转成int8超出了int8的范围,编译时不会报错,只是转换的结果时按溢出处理,
	  和希望的结果不一样.
 */

func main() {
	var i int32 =100
	//将 i => float 需要显示的转换
	var n1 float32= float32(i)
	var n2 int8 = int8(i)
	var n3 int64 = int64(i)  //低精度->高精度
	fmt.Printf("i=%v  n1=%v n2=%v n3=%v \n",i,n1,n2,n3)

	var num1 int64= 999999
	var num2 int8 =int8(num1)
	fmt.Println("num2=",num2)

	var n4 int64
	var n5 int64=n4+1
	fmt.Println("n5=",n5)
	// 运算时不同的类型不同也需要进行转换
	var n6 int32=12
	var n7 int64=int64(n6)+12
	fmt.Println("n7=",n7)

	var n8 int32=12
	var n9 int8=int8(n8)+127  //编译通过但是溢出
	// var n9 int8=int8(n8)+128  //编译不通过
	fmt.Println("n9=",n9)

	// 会 转换失败报错 并且 number 为0
	 str4 :="hello"
	 var number int64=11
	 var err error
	number,err=strconv.ParseInt(str4,10,64)
	fmt.Println("number=",number,"error",err)




}
