package main

import (
	"fmt"
	"strconv"
)

func main() {

	var str string = "true"
	var b bool
	// ParseBool 会返回2个值 (bool, error)
	//  无法转换则会默认为false ,需略error 使用 _ 进行忽略
	b, _ = strconv.ParseBool(str)
	fmt.Printf("b type %T b=%v \n", b, b)

	var str2 string = "1234590"
	var n1 int64
	var n2 int
	// str2 转成10进制的  0转换成int
	n1, _ = strconv.ParseInt(str2, 10, 0)
	n2 = int(n1)
	fmt.Printf("n1 type %T n1=%v \n", n1, n1)
	fmt.Printf("n2 type %T n2=%v \n", n2, n2)
	var str3 string = "123.456"
	var f1 float64
	// 如果无法转换则默认为0
	f1, _ = strconv.ParseFloat(str3, 64)
	fmt.Printf("f1 type %T f1=%v \n", f1, f1)
	var str4 string = "hello"
	// hello无法转换成 int 则变成默认值0 ,str4   转换成10进制   32位 的int类型
	var n3, err = strconv.ParseInt(str4, 10, 32)
	fmt.Printf("n3 type %T n3=%v \n", n3, n3)
	fmt.Printf(err.Error())

}
