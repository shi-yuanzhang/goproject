package main

import (
	"fmt"
	"reflect"
)

type Person struct {
	name string
	age  int
}

func main() {
	person := Person{
		"哈士奇",
		3}
	// 获取vale
	val := reflect.ValueOf(person)

	// 转换Interface
	inter := val.Interface()
	// 断言
	per := inter.(Person)
	fmt.Println(per)

}
