package reflectDemo2

import (
	"fmt"
	"reflect"
	"testing"
)

func TestReflect(t *testing.T) {
	var (
		model *user
		sv    reflect.Value
	)
	// 反射操作结构类型
	model = &user{"1", "哈士奇", "18", "10Kg"}
	sv = reflect.ValueOf(model)
	// 获取它的类型  ,ptr代表指针
	t.Log("reflect.ValueOf", sv.Kind().String())
	// 返回value所包含的指针或者值,必须是指针或者接口
	sv = sv.Elem()
	t.Log("reflect.ValueOf.Elem", sv.Kind().String())
	userId := sv.FieldByName("UserId").String()
	fmt.Printf("userId=%v \n", userId)
	name := sv.FieldByName("Name").String()
	fmt.Printf("name=%v \n", name)
	age := sv.FieldByName("Age").String()
	fmt.Printf("Age=%v \n", age)
	sv.FieldByName("UserId").SetString("2")
	userId = sv.FieldByName("UserId").String()
	fmt.Printf("userId=%v \n", userId)

	weight := sv.FieldByName("weight").String()
	fmt.Printf("weight=%v \n", weight)
	t.Log("---------------------------------")
	// &{2 哈士奇 18 10Kg}
	t.Log("model", model)
	// 反射创建结构体  *reflectDemo2.user
	st := reflect.TypeOf(model)
	t.Log("st type", st)
	// st指向的类型
	st = st.Elem()
	// reflectDemo2.user
	t.Log("st Elem", st)
	// New 返回一个Value类型值,该值有一个指向类型为type的新申请的的零值的指针

	elem := reflect.New(st)
	// elem &{   }
	t.Log("reflect.New   elem", elem)
	// elem &{   }
	model = elem.Interface().(*user)
	t.Log("elem.Interface()   model", elem)
	// 取得elem指向的值
	elem = elem.Elem()
	elem.FieldByName("UserId").SetString("3")

	elem.FieldByName("Name").SetString("阿拉斯加")
	elem.FieldByName("Age").SetString("18")

	t.Log("elem", elem)
}
