package main

import "fmt"

/**
切片,自动扩容功能,可变长度的数组,java的List
var slice []string
var slice1 = []string{"1"}
引用类型,可以通过make来创建
type 类型,len容量大小
make([]type,len)
*/
func main() {
	// 空切片,只是声明没有分配内存
	var slice []string
	// cap如果为null 则为0 cap内置函数,用于统计切片的容量,最大可以存放多少个元素.
	fmt.Printf("len: %v cap %d \n", len(slice), cap(slice))
	fmt.Println(slice)
	var slice1 = []string{"1", "2", "3", "4", "5"}
	fmt.Println(slice1)
	slice1 = append(slice1, "2")
	fmt.Println(slice1)
	var slice2 = make([]int, 2)
	slice2[0] = 1
	slice2[1] = 2
	slice2 = append(slice2, 3)
	fmt.Println("slice2", slice2)
	// 取下标2~5的元素
	slice3 := slice1[2:5]
	fmt.Printf("slice3 %v \n", slice3)
	// 取下标0~5的元素
	slice4 := slice1[:5]
	fmt.Printf("slice4 %v \n", slice4)
	// 取所有的元素
	slice5 := slice1[:]
	fmt.Printf("slice5 %v \n", slice5)

	// 取下标从1开始之后的所有的元素
	slice6 := slice1[1:]
	fmt.Printf("slice6 %v \n", slice6)

}
