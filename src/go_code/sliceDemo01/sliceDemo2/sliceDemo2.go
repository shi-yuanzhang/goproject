package main

import "fmt"

func main() {
	var slice1 = []int{1, 2, 3, 4, 5}
	for i := range slice1 {
		fmt.Println("i:", i)
	}
	for i, i2 := range slice1 {
		fmt.Printf("slice1 [%v]%v,\n", i, i2)
	}

	for i := 0; i < len(slice1); i++ {
		fmt.Printf("slice1 [%v]%v,\n", i, slice1[i])
	}

}
