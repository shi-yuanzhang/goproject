package main

import "fmt"

func main() {

	var slice1 = []int{1, 2, 3, 4, 5}
	// 添加
	slice1 = append(slice1, 6)
	slice1 = append(slice1, 7)
	// cap内置函数,用于统计切片的容量,最大可以存放多少个元素.
	fmt.Println("cap,", cap(slice1))
	fmt.Println(slice1)
	// 移除....  [:1] 只保留 下标0 ,  [2:] 从下标2开始到下标末尾的元素保留
	slice1 = append(slice1[:1], slice1[2:]...)
	fmt.Println(slice1)

	// 引用影响的是地址
	var slice2 = slice1
	// 修改后收到影响
	slice1[0] = 2
	fmt.Println("slice2", slice2)
	fmt.Println("slice1", slice1)
	fmt.Println("------拷贝后------------", len(slice1))
	// 不能var slice3 []int 只声明, 需要 make分配内存
	var slice3 = make([]int, len(slice1))
	// 把slice1 拷贝到slice3,修改slice3不会影响到slice1
	copy(slice3, slice1)
	slice1[0] = 1
	slice3[0] = 3
	fmt.Println("slice3", slice3)
	fmt.Println("slice1", slice1)
}
