package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {

	str := "hello一"
	// 1.len() 获取长度, 统一编码为UTF-8,ascii的字符(字母和数字) 占一个字节,汉字占3个字节
	fmt.Println("str len=", len(str))

	str1 := "hello二"
	// 2.字符串遍历,同时处理有中文的问题 r:=[]rune(str1)
	fmt.Println("str1 rune=", []rune(str1))
	r := []rune(str1)
	for i := 0; i < len(r); i++ {
		fmt.Printf("字符串=%c \n", r[i])
	}

	// 3.字符串转整数: n,err := strconv.Atoi("12")
	// Atoi  Ascii to Integer
	n, err := strconv.Atoi("12")
	if err != nil {
		fmt.Println("转换错误 ", err)
	} else {
		fmt.Println("转成的结果是", n)
	}
	// 4.整数转换字符串strconv.Itoa()
	// Itoa   Integer to Ascii
	n1 := strconv.Itoa(12)
	fmt.Println("转成的结果是 ", n1)

	// 5.字符串 切片 转[]byte var bytes=[]byte("hello go 一")
	// []byte("") 强制转换
	var bytes = []byte("hello go 一")
	// [104 101 108 108 111 32 103 111 32 228 184 128]  32 228 184 128 为 一
	fmt.Printf("bytes=%v \n", bytes)

	// 6.[]byte 转字符串 :str=string([]byte{97,98,99})
	str = string([]byte{32, 228, 184, 128})
	fmt.Println("str=", str)

	// 7.查找字串是否在指定的字符串中: strings.Contains("seafood","foo") //true
	b := strings.Contains("seafood", "foo")
	fmt.Printf("b=%v \n", b)

	// 8.统计一个字符串中有几个指定则字串 strings.Count("ceheese","e") 4
	num := strings.Count("ceheese", "e")
	fmt.Printf("num=%v \n", num)

	// 9. 不区分大小写的字符串比较(== 是区分字母大小写的):fmt.Println(strings.EqualFold("abc","ABC"))
	b1 := strings.EqualFold("abc", "ABC")
	fmt.Printf("abc == ABC  %v\n", b1)
	// 没有返回-1
	fmt.Println("获取出现的下标位置", strings.Index("NLT_abc", "abc"))
	// strings.LastIndex("go go go","go")
	fmt.Println("获取最后出现的下标位置", strings.LastIndex("go go go", "go"))
}
