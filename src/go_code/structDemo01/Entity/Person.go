package Entity

type Person struct {
	id    int
	name  string
	age   int
	email string
}
