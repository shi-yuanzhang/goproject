package main

import (
	"fmt"
)

type Person struct {
	id    int
	name  string
	age   int
	email string
	/**
	嵌套
	*/
	dog Dog
}

type Dog struct {
	name string
}

/**
go 语言没有面向对象的概念了,
但是可以使用结构体来实现,
面向对象编程的一些特性,例如:继承、组合等特性.
注意:传递形参时 如不加 *指针,传递的参数会拷贝一份,不是同一份地址

初始化方式:
1.键值对方式初始化
2.部分初始化
3.列表初始化
4. new 的方式分配内存 返回的是指针


*/

func showPerson(person Person) {
	person.name = "奇士哈"
	fmt.Printf("showPerson =%v \n", person)
}
func showPerson1(person *Person) {
	person.name = "奇士哈"
	fmt.Printf("showPerson =%v \n", person)
}

func main() {
	var person Person
	var dog = Dog{"白哈士奇"}

	person.id = 1
	person.age = 18
	person.name = "哈士奇"
	person.email = "xxx@gmail.com"
	person.dog = dog
	fmt.Println(person.id)
	showPerson(person)
	showPerson1(&person)
	fmt.Println(person)
	// 匿名结构体
	type Person1 struct {
		id    int
		name  string
		age   int
		email string
	}

	// 键值对方式初始化
	kite := Person1{
		id:    1,
		name:  "哈士奇1",
		age:   20,
		email: "xxx@gmail.com"}

	fmt.Println(kite)
	// 部分初始化
	var kite1 = Person1{}
	fmt.Println(kite1)

	// 列表初始化
	kite2 := Person1{
		1,
		"哈士奇1",
		20,
		"xxx@gmail.com"}
	fmt.Println(kite2)

	// new 的方式分配内存 返回的是指针
	kite3 := new(Person1)
	kite3.id = 1
	fmt.Println(kite3)

}
