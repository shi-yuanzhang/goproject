package main

import "fmt"

/**
Go中的方法,是一种特殊的函数,定义与struct之上(与struct关联、绑定),
被称为struct的接受者(receiver).
通俗的讲,方法就是有接受者的函数.
可以理解为OOP的封装,但不完全是
type struct_name struct{}
func(recv struct_name) method_name(para) return_type{}
func{recv *mytype} my_method(para) return_type{}

1.receiver type并非一定要是struct类型,type定义的类型别名,int,slice,map,channel,func类型等都可以.
2.struct结合它的方法就等价于面向对象中的类. 只不过struct可以和它的方法分开,并且一定要属于同一个文件,单必须属于同行一个包.
3.方法有两种接收类型: (T type) 值类型传递 性能差 和(T *Type) 地址传递,他们之间有区别.
4,方法就是函数,所以Go中没有方法重载(overload)的说法,也就是说同一个类型中的所有方法名必须都是唯一.
5.如果receiver是一个指针类型,则会自动解除引用.
6.方法和type是分开的,意味着实例的行为(behavior)和数据存储是分开的,但是它们通过receiver建立起关联关系
7.如果一个类型实现了string()这个方法,那么fmt.Println()会输出string()的结果
8.对于普通函数(func xxxx( x X) ),接受者为值类型时,不能将指针类型的数据直接传递.
9.对于方法(如struct的方法),接受者为值类型时,可以直接用指针类型的变量调用方法.
	不管调用形式如何,真正决定是值拷贝还是地址拷贝,看这个方法是和哪个类型(值还是指针)绑定
*/

type Person struct {
	name string
}

func (per Person) eat() {
	fmt.Printf("%v eat.....\n", per.name)
}

func (per Person) sleep() {
	fmt.Printf("%v,sleep.....\n", per.name)
}

type Customer struct {
	name string
	pwd  string
}

func (customer Customer) login() bool {
	if customer.name == "zsy" && customer.pwd == "123" {
		return true
	}
	return false
}

func main() {
	per := Person{
		name: "哈士奇",
	}

	per.eat()
	per.sleep()
	customer := Customer{
		"zsy",
		"123"}

	flag := customer.login()
	fmt.Println(flag)

}
