package main

import "fmt"

type Person struct {
	name string
}

func showPerson(per Person) {
	per.name = "哈士奇1"
}

func showPerson1(per *Person) {
	// per.name 自定解引用
	// (*per).name ="哈士奇"
	per.name = "哈士奇1"
}

/**
接受者 没有指针,不会修改原地址
*/
func (per Person) showPerson2() {
	per.name = "哈士奇2"
}

/**
接受者 指针,会影响到传入者参数的值
*/
func (per *Person) showPerson3() {
	per.name = "哈士奇2"
}

func main() {
	// 值类型
	p1 := Person{name: "哈士奇"}
	fmt.Printf("p1: %T \n", p1)

	// 指针类型
	p2 := &Person{name: "哈士奇"}
	fmt.Printf("p2: %T \n", p2)
	fmt.Println("--------------------")
	// 没有被修改
	showPerson(p1)
	fmt.Printf("p1: %v \n", p1)
	// 被修改
	showPerson1(p2)
	fmt.Printf("p2: %v \n", p2)

	fmt.Println("--------------------")
	// showPerson2 修改值 p1不受影响
	p1.showPerson2()
	fmt.Printf("p1: %v \n", p1)
	// showPerson3 指针 修改值 p2不受影响
	p2.showPerson3()
	fmt.Printf("p2: %v \n", p2)

}
