package main

import "fmt"

/**
golang中不存在继承,重写等功能
*/
type Dog struct {
	// golang的另类的方式,当作继承来使用..
	Person
	name string
}

type Person struct {
	name string
	age  int
}

// 实现string()这个方法 mt.Println()会输出string()的结果
func (dog *Dog) String() string {
	s := fmt.Sprintf("name=[%v]", dog.name)
	return s
}

func (per Person) run() {
	fmt.Printf("%v 开始跑了起来......\n", per.name)
}

func (dog Dog) operation() {
	fmt.Printf("%v牵着人类.... \n", dog.name)
	dog.run()
}

func main() {
	person := Person{"人类1号", 24}

	dog := Dog{person, "哈士奇"}
	// 加上&才会调用String
	fmt.Println(&dog)
	// 从形式上看是传入了地址,但实际还是值拷贝
	(&dog).operation()

}
