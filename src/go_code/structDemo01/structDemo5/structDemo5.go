package main

import "fmt"

/**
在两个结构体 的字段相同时,可以进行相互强转
*/
type A struct {
	number int
}

type B struct {
	number int
}

func main() {
	var a A
	var b = B{1}
	a = A(b)
	fmt.Println(a)

}
