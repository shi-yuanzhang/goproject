package main

import (
 "fmt"
)

/**
	值类型: 基本数据类型int系列,float系列,bool,string、数组和结构体struct
			变量直接存储值, 内存通常在栈中分配

	引用类型: 指针、slice切片、map、管道chan、interface等都是引用类型
			变量存储的是一个地址,这个地址对应的空间才真正存储数据,内存通常在堆上分配,
			当没有任何变量引用这个地址时,该地址对应的数据空间就成为一个垃圾,由GC来回收
 */
func main() {
 var a_ int8=1
 fmt.Print(a_)



}
