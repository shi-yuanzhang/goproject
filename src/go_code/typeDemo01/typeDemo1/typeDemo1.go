package main

import "fmt"

/**
类型定义和类型别名
类型定义的语法
type NewType Type
*/
func main() {

	// 类型定义
	type MyInt int
	// i为MyInt类型
	var i MyInt
	i = 100
	fmt.Printf("i:%v i: %T \n", i, i)

}
