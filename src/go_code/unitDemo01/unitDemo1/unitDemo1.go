package unitDemo1

import "fmt"

/**
单元测试
Golang中自带有一个轻量级的测试框架testing和自带的 go test命令来实现单元测试和性能测试,
testing框架和其他语言中的测试框架类似,可以基于这个框架写针对相应函数的测试用例,也可以
基于该框架写相应的压力测试用例.
单元测试确保运行结果的正确性,性能.
*/

func Add(a int, b int) int {
	fmt.Println("Add......")
	return a + b
}
