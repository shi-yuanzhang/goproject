package unitDemo1

import (
	"testing"
)

/**
testing提供对Go包的自动化测试的支持,通过'go test'命令,能够自动执行如下形式的任何函数:
func TestXxx(t *testing.T)
其中Xxx可以是任何字母数字字符串(但第一个不能小写),用于识别测试例程.
在这些函数中,使用Error,Fail或者相关方法来发送失败信号.
要在编写一个新的测试套件,需要创建一个名为 _test.go结尾的文件,该文件包含'TestXxx'函数.
将该文件放在与被测试的包相同的包中.该文件将被排除在正常的程序包之外,但在运行'go test'命令时将
被包含.有关详细信息,请运行'go help test'和'go help testflag'了解.

如果需要,可以调用*.T和*.B的Skip方法,跳过该测试或基准测试:
if testing.Short(){
	t.Skip("Skipping test in short mode.")
}
testing框架
1.将xxx_test.go的文件引入
2.调用TestXxx()函数
3.Testxxx(t *testing.T) 的形参类型必须是 *testing.T
4.运行测试用例执行
	1)cmd >go test 如果运行正确,无日志.错误时,会输出日志
	2)cmd >go test -v 运行正确或是错误,都输出日志
	3)测试单个文件 go test -v xxx_test.go xxx.go
		 例子 go test -v .\unitDemo1_test.go .\unitDemo1.go
	4)测试单个方法  新版 go test -v -run=TestXxx    旧版go test -v -test.run TestXxx
		 例子  go test -v -run=TestAdd
5.Fatalf格式化输出错误信息,并退出程序.
6.t.Logf方法可以输出相应的日志.
7.Pass标识运行成功,Fail表示运行失败.

*/

// 编写测试用例,Test开头 为单元测试,可以直接启动不需要main方法
// testing框架包  T类型 是testing包一个结构体,该结构体提供了很多方法
func TestAdd(t *testing.T) {
	number := Add(1, 2)
	// testing提供的输入日志
	t.Log(number)
	//输出错误时 在调用Logf输出日志之后调用FailNow停止  Fatalf
	//t.Fatal(number)
}
