package main

import (
	"fmt"
	"github.com/garyburd/redigo/redis"
)

func main() {

	conn, err := redis.Dial("tcp", "127.0.0.1:6379")
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	defer conn.Close()
	conn.Do("set", "name", "哈士奇")

	// 转换类型输出或者使用redis.String()
	val, err1 := redis.String(conn.Do("get", "name"))

	if err1 != nil {
		fmt.Println(err.Error())
		return
	}
	// 转换类型输出或者使用redis.String()
	//type1, _ := val.([]byte)
	//fmt.Println(string(type1))
	fmt.Println(val)

}
