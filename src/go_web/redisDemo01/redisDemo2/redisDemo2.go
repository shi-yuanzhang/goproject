package main

import (
	"fmt"
	"github.com/garyburd/redigo/redis"
)

func main() {
	conn, err := redis.Dial("tcp", "127.0.0.1:6379")
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	defer conn.Close()
	// 通过go向redis写入数据 string[key-val]
	_, err = conn.Do("HSet", "user01", "name", "哈士奇")
	if err != nil {
		fmt.Println("Hset err=", err)
		return
	}
	_, err = conn.Do("HSet", "user01", "age", 2)
	if err != nil {
		fmt.Println("Hset err=", err)
		return
	}

	name, err := redis.String(conn.Do("HGet", "user01", "name"))
	if err != nil {
		fmt.Println("Hset err=", err)
		return
	}

	age, err := redis.Int(conn.Do("HGet", "user01", "age"))
	if err != nil {
		fmt.Println("Hset err=", err)
		return
	}

	fmt.Printf("name %v  age %v\n", name, age)

	// 一次性取出数据  redis.Strings
	user, err := redis.Strings(conn.Do("HMGet", "user01", "name", "age"))
	if err != nil {
		fmt.Println("Hset err=", err)
		return
	}

	for i, value := range user {
		fmt.Printf("i=%v value=%v \n", i, value)
	}

}
